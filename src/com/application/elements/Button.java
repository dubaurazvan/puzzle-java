package com.application.elements;

import javax.swing.JButton;

public class Button extends JButton {
	
	public String id;

	public Button(String label, String id) {
		this.id = id;
		this.setText(label);
	}
	
}
