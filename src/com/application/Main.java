package com.application;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;

public class Main extends JPanel implements MouseListener, KeyListener {
   
	private static final long serialVersionUID = -3599084435349716519L;
	public static int TimerStep = 1;
	public static Timer timer;
	public static Countdown countdown;
	
	private void drawString(Graphics g, String text, int x, int y) {
        for (String line : text.split("\n"))
            g.drawString(line, x, y += g.getFontMetrics().getHeight());
    }
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		customPaint(g);
	}
	
	public void customPaint(Graphics g) {
		g.setFont(new Font("",0,16));
		//FontMetrics fm = getFontMetrics(new Font("",0,16));
		for (int i = 1; i < Piece.pieces.length; i++) {
			int finalPos = i;
		    int currentPos = Piece.pieces[i];
		    
		    if (Piece.isFinished == true) { //draw a success message
		    	g.setColor(Color.white);
				g.fillRect( 0, 0, 450, 450);
				
				g.setFont(new Font("",0,24));
				g.setColor(Color.green);
				drawString(g, "You finished the puzzle!\nCongratulations!", 100, 100);
		    } else { //still playing / drawing
		    	g.setColor(Color.gray);
				/*g.fillRoundRect( Piece.getX(currentPos), Piece.getY(currentPos), Piece.width, Piece.height, 10, 10);
				
				g.setColor(Color.black);
				g.drawRoundRect( Piece.getX(currentPos), Piece.getY(currentPos), Piece.width, Piece.height, 10, 10);
				g.setColor(Color.white);*/
		    	Image img1 = Toolkit.getDefaultToolkit().getImage("Chrysanthemum.jpg");
		    	
		    	g.drawImage(img1
		    					, Piece.getX(currentPos)
		    					, Piece.getY(currentPos)
		    					, Piece.getX(currentPos) + Piece.width
		    					, Piece.getY(currentPos) + Piece.height
		    					, Piece.getX(finalPos, false)
		    					, Piece.getY(finalPos, false)
		    					, Piece.getX(finalPos, false) + Piece.width
		    					, Piece.getY(finalPos, false) + Piece.height
		    					, null);
				
		    	g.setColor(Color.black);
				g.drawString(String.valueOf(finalPos), Piece.getX(currentPos) + Piece.width/2, Piece.getY(currentPos) + Piece.height/2);
		    }
		}
		
		// Countdown draw
		g.setColor(Color.white);
		g.fillRect( 0, 490, this.getWidth(), 100);
		
		g.setColor(Color.black);
		g.setFont(new Font("",0,24));
		
		g.drawString(countdown.getLabel() , 200, 520);
		
		if (countdown instanceof Countdown) {
			g.setColor(Color.white);
			g.fillRect( 0, 0, this.getWidth(), 400);
			
			g.setColor(Color.black);
			g.drawString("Game Over", 100, 100);
		}
	}
	
	public static JFrame myFrame;
	public Main() {
		//Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
		final JFrame myFrame = new JFrame("Puzzle Java Graphics");
		myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container myPane = myFrame.getContentPane();
		myPane.setLayout(null);
		
		myFrame.setBackground(Color.white);
		myFrame.setSize(470, 590);
		myFrame.setVisible(true);
		myFrame.setContentPane(this);
		myFrame.addMouseListener(this);
		myFrame.addKeyListener(this);
		
		new Piece();
		
		//countdown = new Countdown(20);
		
		System.out.println(countdown instanceof Countdown);
		
		//countdown.start(20);
		
		
		run();
	}
	
	public void run() {
	   while (true) {
	      repaint();
	      try {
	         Thread.sleep(17);
	      } catch (InterruptedException e) {
	         e.printStackTrace();
	      }
	   }
	}
	
	
	public static void main(String[] args) {
		new Main();
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		if (Piece.inMove == 0) {
			for (int i = 1; i < Piece.rows*Piece.columns; i++) {
				if (Piece.rect[i].contains(e.getPoint())) {
					Piece.move(Piece.pieces[i],Piece.getEmptyPos());
				}
			}
		}
		
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		//System.out.println("mouseEntered");
	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		//System.out.println("mouseExited");
	}
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		//System.out.println("mousePressed");
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		//System.out.println("mouseReleased");
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
			case KeyEvent.VK_UP:
				if (Piece.getEmptyPos() + Piece.rows <= (Piece.rows * Piece.columns)) {
					Piece.move(Piece.getEmptyPos() + Piece.rows ,Piece.getEmptyPos());
				}
				break;
	
			case KeyEvent.VK_DOWN:
				if (Piece.getEmptyPos() - Piece.rows > 0) {
					Piece.move(Piece.getEmptyPos() - Piece.rows ,Piece.getEmptyPos());
				}
				break;
	
			case KeyEvent.VK_LEFT:
				if (Piece.getColumn(Piece.getEmptyPos() + 1) <= Piece.columns) {
					Piece.move(Piece.getEmptyPos() + 1 ,Piece.getEmptyPos());
				}
				break;
	
			case KeyEvent.VK_RIGHT:
				if (Piece.getColumn(Piece.getEmptyPos() - 1) > 0) {
					Piece.move(Piece.getEmptyPos() - 1 ,Piece.getEmptyPos());
				}
				break;
		}
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
	}

   
	public void log(Object obj) {
		System.out.println(obj);
	}
	
}
