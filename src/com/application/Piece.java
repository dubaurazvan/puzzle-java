package com.application;

//import java.awt.Color;
import java.awt.Rectangle;
import java.util.Timer;
import java.util.TimerTask;

public class Piece {

	//public static HashMap<finalPosition, currentPosition> pieces;
	public static int rows = 3;
	public static int columns = 3;
	public static Rectangle[] rect = new Rectangle[rows*columns];
	public static int pieces[] = new int[rows*columns];
	public static int width = 150; 
	public static int height = 150; 
	public static int inMove = 0;
	public static double inMoveX = 0; 
	public static double inMoveY = 0; 
	public static boolean isFinished = false;

	public Piece() {
		if (rows > 3) {
			width = 110;
			height = 110;
		}
		
		pieces[1] = 3;
		pieces[2] = 7;
		pieces[3] = 4;
		pieces[4] = 5;
		pieces[5] = 1;
		pieces[6] = 2;
		pieces[7] = 8;
		pieces[8] = 6;
		
		if (rows > 3) {
			
			
			pieces[9] = 9;
			pieces[10] = 12;
			pieces[11] = 13;
			pieces[12] = 15;
			pieces[13] = 11;
			pieces[14] = 10;
			pieces[15] = 14;
		}
		
		for (int i = 1; i < rows*columns; i++) {
			rect[i] = new Rectangle();
			rect[i].setBounds(getX(pieces[i]), getY(pieces[i]), width, height);
		}
		
	}
	
	public static void updateRect(int key) {
		rect[key].setBounds(Piece.getX(pieces[key]), Piece.getY(pieces[key]), Piece.width, Piece.height);
	}
	
	public static int getRow(int pos) {
		return (int) Math.ceil((double) pos / (double) rows);
	}
	
	public static int getColumn(int pos) {
		return pos - ((getRow(pos) - 1) * columns);
	}
	
	public static int getX(int pos) {
		if (inMove == pos) {
			return (int) inMoveX;
		}
		return (int) (((double) getColumn(pos) - 1) * (double) width);
	}
	
	public static int getY(int pos) {
		if (inMove == pos) {
			return (int) inMoveY;
		} 
		return (int) (((double) getRow(pos) - 1) * (double) height);
	}
	
	public static int getX(int pos, boolean noInMove) {
		return (int) (((double) getColumn(pos) - 1) * (double) width);
	}
	
	public static int getY(int pos, boolean noInMove) {
		return (int) (((double) getRow(pos) - 1) * (double) height);
	}
	
	private static Timer timer;
	public static void move(int currentPos, final int newPos) {
		if (checkAvailableMoves(currentPos) == true) {
			
			if (inMove == 0) {
			
				inMoveX = getX(currentPos);
				inMoveY = getY(currentPos);
				inMove = currentPos;
				
				timer = new Timer();
				timer.schedule( new TimerTask() {
					@Override
					public void run() {
						//int newPos = Integer.parseInt(ae.getActionCommand());
						
				    	if ((int) inMoveX == getX(newPos) && (int) inMoveY == getY(newPos)) {
				    		timer.cancel();
				    		
				    		for (int i = 1; i < pieces.length; i++) {
				    		    if (inMove == pieces[i]) {
				    		    	pieces[i] = newPos;
				    		    	updateRect(i);
				    		    	isFinished();
				    		    }
				    		}
				    		
				    		inMove = 0;
				    		inMoveX = 0;
				    		inMoveY = 0;
				    	}
				    	
				    	if (inMoveX < getX(newPos)) {
				    		inMoveX = inMoveX + 10;
				    	} else if (inMoveX > getX(newPos)) {
				    		inMoveX = inMoveX - 10;
				    	}
				    	
				    	if (inMoveY < getY(newPos)) {
				    		inMoveY = inMoveY + 10;
				    	} else if (inMoveY > getY(newPos)) {
				    		inMoveY = inMoveY - 10;
				    	}
				    	
				    }
				},0, 17);
			}
		} else if (checkMultipleAvailableMoves(currentPos) == true) {
			
			
		}
		
		
	}

	public static boolean checkAvailableMoves(int currentPos) {
		if (getRow(currentPos) == getRow(getEmptyPos())) { 
			
			if (getColumn(currentPos)-getColumn(getEmptyPos()) == 1 || getColumn(currentPos)-getColumn(getEmptyPos()) == -1) {
				return true;//makeMove(pieceText); 
			}
			
		} 
		else if (getColumn(currentPos) == getColumn(getEmptyPos())) { 
			
			if (getRow(currentPos) - getRow(getEmptyPos()) == 1 || getRow(currentPos) - getRow(getEmptyPos()) == -1) {
				return true;//makeMove(pieceText);
			}
			
		}
		
		return false;//flash(piecePosition);
		
	}
	
	public static boolean checkMultipleAvailableMoves(int currentPos) {
		if (getRow(currentPos) == getRow(getEmptyPos())) { 
			
			if (getColumn(currentPos)-getColumn(getEmptyPos()) == 1 || getColumn(currentPos)-getColumn(getEmptyPos()) == -1) {
				return true;//makeMove(pieceText); 
			}
			
		} 
		else if (getColumn(currentPos) == getColumn(getEmptyPos())) { 
			
			if (getRow(currentPos) - getRow(getEmptyPos()) == 1 || getRow(currentPos) - getRow(getEmptyPos()) == -1) {
				return true;//makeMove(pieceText);
			}
			
		}
		
		return false;//flash(piecePosition);
		
	}
	
	
	public static int getEmptyPos() {
		int emptyPos = 0;
		for(int i=1; i < rows*columns; i++) {
			
			boolean contains = false;
			for (int j = 1; j < pieces.length; j++) {
				if (pieces[j] == i) {
					contains = true;
				}
			}
			if (contains == false) emptyPos = i;
			
		}
		
		if (emptyPos == 0) emptyPos = rows*columns;
		
		return emptyPos;
	}
	
	
	public static boolean isFinished() {
		boolean response = true;
		
		for (int i = 1; i < pieces.length; i++) {
			if(i != pieces[i]) response = false;
		}
		
		if(response == true) { // the puzzle was resolved
			isFinished = true;
		}
		
		return response;
	}
	
	
}

