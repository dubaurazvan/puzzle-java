package com.application;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.ScrollPane;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import org.json.*;

public class getJson {

	/**
	 * @param args
	 */
	public static JFrame myFrame = new JFrame("FlowLayout Test");
	public static Container myPane = myFrame.getContentPane();
	//public static JPanel page = new JPanel();
	public static void main(String[] args) {
		
		//JScrollPane scrollBar = new JScrollPane(page);  
		
		//set Layout
		myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myFrame.setSize(500, 200);
		myFrame.setBackground(Color.blue);
		
		//myPane.add(page);
		try {
		    // Create a URL for the desired page
		    URL url = new URL("http://www.extragsm.com/_api/mobile-admin/get.comments.php?key=2891bnsiudy1d278d2");
		    BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
		    String str = in.readLine();
		    if (str != null) showList(str);
		    in.close();
		} catch (MalformedURLException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}
		
		
		myFrame.setVisible(true);

	}
	
	public static void showList(String str) {
		
		//set Container
		myPane.setLayout(new GridBagLayout());
		myPane.setBackground(Color.red);
		//myFrame.getContentPane().add(new JScrollPane(myPane), BorderLayout.CENTER);
		
		GridBagConstraints c = new GridBagConstraints();
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.weightx = 1.0;
		c.weighty = 1.0;
		
		JSONObject myList = new JSONObject(str);
	    // System.out.println(str);
        JSONArray commentsList  = myList.getJSONArray("comment-list"); 
        for (int i = 0; i < commentsList.length(); i++) {
        	JSONObject item = (JSONObject) commentsList.get(i); 

        	//do something
        	System.out.println(item.get("id"));
        	
        	JPanel row = new JPanel();
        	row.setBackground(Color.cyan);
        	row.setLayout(new FlowLayout(FlowLayout.LEFT));
        	row.setBorder(new EmptyBorder(20, 20, 20, 20));
        	
        	//row.setBackground(Color.GRAY);
        	//row.setPreferredSize( myFrame.getSize());
        
        	JTextArea comment = new JTextArea();
        	comment.setText((String) item.get("comment_text"));
        	comment.setBackground(Color.green);
        	comment.setEditable(false);
        	comment.setLineWrap(true);  
        	//comment.setBorder(new EmptyBorder(20, 20, 20, 20));
        	
        	
        	JButton delete = new JButton("Remove");
        	delete.setBorder(new EmptyBorder(20, 20, 20, 20));
        	delete.setBackground(Color.WHITE);
        	
        	//row.add(comment);
        	
        	c.gridy = i;
        	//comment.add(delete);
        	myPane.add(comment, c);
			
		}
	}

}
