package com.application;

import java.util.Timer;
import java.util.TimerTask;

public class Countdown { 

	protected static int seconds;
	private static Timer timer;
	public static boolean running = false;
	
	public Countdown(int sec) {
		start(sec);
	}
	
	public static void start(int sec) {
		running = true;
		seconds = sec;
		timer = new Timer();
		timer.schedule( new TimerTask() {
			@Override
			public void run() {
				seconds--;
				if (seconds == 0) stop();
		    }
		},0, 1000);
	}
	
	public static void stop() {
		running = false;
		timer.cancel();
	}
	
	public static String getLabel() {
		
		int min = (int) Math.floor(seconds / 60);
		String minLabel = (min <10 ? "0" : "") + String.valueOf(min);
		
		// Seconds
		int sec = (int) (seconds - (min * 60));
		String secLabel = (sec <10 ? "0" : "") + String.valueOf(sec); 
		
		return minLabel + ":" + secLabel;
	}
	
}
